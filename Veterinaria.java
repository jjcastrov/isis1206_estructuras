package isis1206_estructuras;
import java.util.Scanner;

public class Veterinaria {
  public Queue<String> gaticos;
  public Queue<String> perritos;

  public Veterinaria() {
    gaticos = new Queue<String>();
    perritos = new Queue<String>();
  }

  public static void error() {
    System.out.println("Error. Entrada inválida.");
  }

  public static void main(String[] args) {
    Veterinaria vet = new Veterinaria();

    Queue<String> perros = vet.perritos;
    Queue<String> gatos = vet.gaticos;

    Scanner sc = new Scanner(System.in);
    

    while(true) {
      System.out.println("Escriba 'agregar' para agregar un perro o gato, o escriba 'sacar' para sacar un perro o un gato.");

      try {
        String l1 = sc.nextLine();

        if(l1.equals("agregar")) {
          System.out.println("Ingrese el nombre de la mascota a agregar");
          String nom = sc.nextLine();
          System.out.println("Ingrese 1 si es un gato, 2 si es un perro.");
          String l2 = sc.nextLine();
          if(l2.equals("1")) gatos.enqueue(nom);
          else if(l2.equals("2")) perros.enqueue(nom);
          else error();
        }
        else if(l1.equals("sacar")) {
          System.out.println("Ingrese 1 si desea sacar un gato, 2 si es un perro.");
          String l2 = sc.nextLine();
          if(l2.equals("1")) {
            System.out.println("Se sacó el gato " + gatos.dequeue());
          }
          else if(l2.equals("2")) {
            System.out.println("Se sacó el perro " + perros.dequeue());
          }
          else error();
        }
        else error();
      } catch(Exception e) {
        error();
      }

    }
  }
}