package isis1206_estructuras;
import java.util.Scanner;

public class Suma{

	class Num{
		private int numero;
		public Num(int num){
			numero=num;
		}
		public int darNum(){
			return numero;
		}
		public int sumar(Num n){
			return numero+n.darNum();
		}
	}

	public Suma(){
		Scanner sc = new Scanner(System.in);
		System.out.print("Ingrese el primer numero: ");
		char[] numeroUno= sc.nextLine().toCharArray();
		System.out.print("Ingrese el segundo numero: ");
		char[] numeroDos = sc.nextLine().toCharArray();
		LinkedList<Num> lista1 = new LinkedList<Num>();
		LinkedList<Num> lista2 = new LinkedList<Num>();
		for(int i=0; i<numeroUno.length;i++){
			lista1.addFirst(new Num(Integer.parseInt(numeroUno[i]+"")));
		}
		for(int i=0; i<numeroDos.length;i++){
			lista2.addFirst(new Num(Integer.parseInt(numeroDos[i]+"")));
		}

		LinkedList<Num> listaResp = sumarListas(lista1,lista2);
		String resp="";
		Num actual = listaResp.removeFirst();
		while(actual!=null){
			resp += actual.darNum();
			actual=listaResp.removeFirst();
		}
		System.out.print(resp);
	}

	public LinkedList<Num> sumarListas(LinkedList<Num> l1, LinkedList<Num> l2){
		Num primero=l1.removeFirst();
		Num segundo=l2.removeFirst();
		LinkedList<Num> resp = new LinkedList<Num>();
		int sumaAcomulada=0;
		while(!(primero==null && segundo==null)){
			if(primero==null){
				primero=new Num(0);
			}else if(segundo==null){
				segundo=new Num(0);
			}
			int suma = primero.sumar(segundo)+sumaAcomulada;
			sumaAcomulada=0;
			System.out.println("Se sumo "+primero.darNum()+" con "+segundo.darNum()+" y dio "+suma);
			if(suma>=10){
				sumaAcomulada++;
				suma=suma%10;
			}
			resp.addFirst(new Num(suma));
			primero=l1.removeFirst();
			segundo=l2.removeFirst();
		}
		if(sumaAcomulada==1){
			resp.addFirst(new Num(1));
		}
		return resp;
	}






	public static void main(String[] args) {
		Suma s = new Suma();
	}
}