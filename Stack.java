package isis1206_estructuras;

public class Stack<T> {
  
  private class Node {
    private T data;
    private Node next;

    public Node(T data) {
      this.data = data;
    }
  }

  private Node head;
  private int size;

  public Stack() {
    head = null;
    size = 0;
  }

  public void push(T data) {
    Node newNode = new Node(data);
    newNode.next = head;
    
    size++;
    head = newNode;
  }

  public T pop() {
    Node removed = head;
    
    if(head != null)
      head = head.next;

    if(size > 0) size--;
    return removed != null ? removed.data : null;
  }
}