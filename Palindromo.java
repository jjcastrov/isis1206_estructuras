package isis1206_estructuras;
import java.util.Scanner;
public class Palindromo{

	class Character{
		char character;
		public Character(char c){
			character=c;
		}
		public char darChar(){
			return character;
		}
		public boolean equals(Character c){
			return c.darChar()==character;
		}
	}

	public static void main(String[] args) {
		Palindromo p = new Palindromo();
	}


	public Palindromo(){
		Scanner sc = new Scanner(System.in);
		System.out.print("Ingrese una palabra para verificar si es palindromo: ");
		String l = sc.nextLine();
		char[] palabra = l.toCharArray();
		DoublyLinkedList<Character> lista = new DoublyLinkedList<Character>();
		for(int i=0; i<palabra.length; i++){
			Character actual = new Character(palabra[i]);
			lista.addLast(actual);
		}
		System.out.print(verificarPalindromo(lista));
	}

	public boolean verificarPalindromo(DoublyLinkedList lista){
		Character actualPrimera;
		Character actualUltima;
		boolean esPalindromo=true;;
		for(int i=0; i<(int)lista.size()/2 && esPalindromo;i++){
			actualPrimera=(Character)lista.removeFirst();
			actualUltima=(Character)lista.removeLast();
			if(!actualPrimera.equals(actualUltima)){
				esPalindromo=false;
			}
		}
		return esPalindromo;
	}
}