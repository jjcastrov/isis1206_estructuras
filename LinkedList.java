package isis1206_estructuras;

public class LinkedList<T> {
  
  private class Node {
    private T data;
    private Node next;

    public Node(T data) {
      this.data = data;
    }
  }

  private Node head, tail;
  private int size;

  public LinkedList() {
    head = null;
    tail = null;
    size = 0;
  }

  public int size(){
    return size;
  }
  
  public void addLast(T data) {
    Node newNode = new Node(data);
    
    if(head == null)
      head = newNode;
    else
      tail.next = newNode;

    tail = newNode;
    size++;
  }

  public void addFirst(T data) {
    Node newNode = new Node(data);
    newNode.next = head;
    
    if(tail == null)
      tail = newNode;
    
    size++;
    head = newNode;
  }

  public T removeFirst() {
    Node removed = head;
    
    if(head != null)
      head = head.next;
    
    if(head == null)
      tail = null;

    if(size > 0) size--;
    return removed != null ? removed.data : null;
  }

  public T removeLast() {
    Node removed = tail;

    if(head != null && head == tail) {
      head = null;
      tail = null;
    } else {
      Node i = head;
      while(i != null) {
        if(i.next == tail) {
          i.next = null;
          tail = i;
          break;
        }
        i = i.next;
      }
    }

    if(size > 0) size--;
    return removed != null ? removed.data : null;
  }
}