package isis1206_estructuras;

public class Queue<T> {
  
  private class Node {
    private T data;
    private Node next;

    public Node(T data) {
      this.data = data;
    }
  }

  private Node head, tail;
  private int size;

  public Queue() {
    head = null;
    tail = null;
    size = 0;
  }

  public void enqueue(T data) {
    Node newNode = new Node(data);
    newNode.next = head;
    
    if(tail == null)
      tail = newNode;
    
    size++;
    head = newNode;
  }

  public T dequeue() {
    Node removed = tail;

    if(head != null && head == tail) {
      head = null;
      tail = null;
    } else {
      Node i = head;
      while(i != null) {
        if(i.next == tail) {
          i.next = null;
          tail = i;
          break;
        }
        i = i.next;
      }
    }

    if(size > 0) size--;
    return removed != null ? removed.data : null;
  }
}