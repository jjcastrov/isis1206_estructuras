package isis1206_estructuras;
import java.util.Scanner;

public class PilaMinimo<T extends Comparable<T>> {
    
  private class Node {
    private T data;
    private Node next;
    private T prevMin;

    public Node(T data, T prevMin) {
      this.data = data;
      this.prevMin = prevMin;
    }
  }

  private Node head;
  private int size;
  private T min;

  public PilaMinimo() {
    head = null;
    size = 0;
    min = null;
  }

  public void push(T data) {
    Node newNode = new Node(data, min);
    newNode.next = head;

    if(min == null) {
      min = data;
    } else if(data.compareTo(min) < 0)
      min = data;
    
    size++;
    head = newNode;
  }

  public T pop() {
    Node removed = head;
    
    if(head != null) {
      head = head.next;
      min = removed.prevMin;
    }

    if(size > 0) size--;
    return removed != null ? removed.data : null;
  }

  public void mostrar() {
    Node i = head;

    System.out.print("H -> ");

    while(i != null) {
      System.out.print(i.data + " -> ");
      i = i.next;
    }

    System.out.print("null.");
    System.out.println();
  }

  public static void main(String[] args) {
    PilaMinimo<Integer> stack = new PilaMinimo<Integer>();
    Scanner sc = new Scanner(System.in);
    System.out.println("Escriba un número para apilar un número, 'pila' para mostrar el estado de la pila actual, 'pop' para desapilar o 'min' para obtener el mínimo actual");
    
    while(true) {
      String l = sc.nextLine();
      try {
        Integer i = Integer.parseInt(l);
        stack.push(i);
        System.out.println("Se ha apilado el número " + i);
      } catch (NumberFormatException e) {
        if(l.equals("pop")) {
          Integer x = stack.pop();
          System.out.println("Se ha desapilado el número " + x);
        }
        else if(l.equals("min")) {
          System.out.println("El menor número es " + stack.min);
        }
        else if(l.equals("pila")) {
          stack.mostrar();
        }
        else {
          System.out.println("Entrada incorrecta.");
        }
      }
    }
  }
}